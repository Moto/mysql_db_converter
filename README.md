
### Скрипт конвертации MySQL базы данных.

Скрипт автоматически конвертирует базу данных в `utf8mb4` и меняет коллацию на
`utf8mb4_0900_ai_ci`.

```sh
./db_conv.sh <db_user> <db_pass> <db_name> <old_collate>
```
Пример:
```sh
./db_conv.sh mysql_user my_db_pass somedatabase latin1
```
