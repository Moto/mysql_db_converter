#!/bin/bash

#######################################################################  
# Description: Change DB character set, convert tables
#              and columns to utf8mb4 character set and
#              change collation to utf8mb4_0900_ai_ci.
# Author:      Krasheninnikov A.S.
# Version:     v0.1 (02-09-2020)
# Usage:       ./db_conv.sh <db_user> <db_pass> <db_name> <old_collate>
#######################################################################

USER=$1
PASSWORD=$2
DB_NAME=$3
OLD_COLL=$4
CHARACTER_SET="utf8mb4"
COLLATE="utf8mb4_0900_ai_ci"

if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ] || [ -z "$4" ]
then
    echo "Error: Some parameters was not set."
    echo "Help: db_conv.sh <DB user_name> <Password> <DB name> <old collate>"
    exit
else
  echo "ALTER DATABASE ${DB_NAME} CHARACTER SET ${CHARACTER_SET} COLLATE ${COLLATE};" | mysql --user=$USER --password=$PASSWORD
  tables=`mysql --user=$USER --password=$PASSWORD $DB_NAME -e "SELECT tbl.TABLE_NAME FROM information_schema.TABLES tbl WHERE tbl.table_collation = '${OLD_COLL}' AND tbl.TABLE_TYPE='BASE TABLE' AND table_schema = '${DB_NAME}';"`

  for tableName in $tables; do
      if [[ "$tableName" != "TABLE_NAME" ]] ; then
          mysql --user=$USER --password=$PASSWORD -e "ALTER TABLE ${DB_NAME}.${tableName} CONVERT TO CHARACTER SET ${CHARACTER_SET} COLLATE ${COLLATE};"
          echo "$tableName - done"
      fi
  done
fi 
